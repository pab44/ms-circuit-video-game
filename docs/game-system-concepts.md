# Game System Concepts

List of concepts that are useful to know for this project.

## Memory Independent Video Games
Video games that do not depend entirely on code in memory to drive the system
and operations of how logic, rendering, and sound of a video game is performed.

## Locally-Generated Assets
Assets(sound, images, graphics) that are generated internally in the system.